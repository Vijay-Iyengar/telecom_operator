# Telecom operator API

An Telecom operator API for people who like to activate phone numbers via APIs.

## System Requirements

* Needs `docker` and `docker-compose` to be installed.
* Does not require any local tooling like JDK, mvn, etc., as it all runs inside Docker.

## How to run

* Use `./auto/dev-environment` to get a local environment to run your dev tools in.
  JDK is present in that environment, use `./mvnw` for maven.
* `./auto/test`, `./auto/package` and `./auto/build` are intended to become CI/CD steps.
* `./auto/run` can be used to simulate the CI/CD pipeline locally
  and then run the app on http://localhost:8080.

## How to use


### Read all phone numbers from one address book

GET `"/customers/phone-numbers"`

<details>
<summary>Sample Response Body</summary>

```json
{
  "customerList": [
    {
      "id": 1,
      "name": "Mason",
      "phoneNumbers": [
        "(04) 9720 3766"
      ]
    },
    {
      "id": 2,
      "name": "Baker",
      "phoneNumbers": [
        "(02) 3793 7597",
        "(01) 0161 1510"
      ]
    }
  ]
}

```

</details>


### Read all Phone Number for a contact

GET `"/customers/{customer-id}/phone-numbers"`

<details>
<summary>Sample Response Body</summary>

```json
[
  "(04) 9720 3766"
]

```

</details>

Returns 404 if Customer does not exist.

### Activate PhoneNumber, returning Accepted as Response status

PUT `"/customers/{customer-id}/phone-numbers/activate"`

<details>
<summary>Sample Response Body</summary>
Returns 202
</details>

Returns 404 if contact does not exist.

**Note:** The Customer ID can be determined in two ways:

* Reading all phoneNumbers returns a mapping
  that has the Customer Id.

## Project Specifications

**Acceptance Criteria**

* Address book will hold name and phone numbers of contact entries
  * get all phone numbers
  * get all phone numbers of a single customer
  * activate a phone number
  
## Assumptions

* Reviewer is running Mac or Linux (scripts are in bash).
* I left out features that were not in the requirements:
    * Create/Update/Delete an existing Customer/PhoneNumber(except Activate),

## Design Decisions and Comments
*  phone numbers can be formatted in different ways, include country modifiers, etc.,
  so I chose a simple string again and didn't do much validation on format.

## Possible Extensions / After-thoughts

* As I could use modern Java's `record` classes, but would have liked to use it.
* There were no requirements for security, but one should probably add user validation
  before productionising a multitenancy system.
* Add OpenAPI specification for the API and run SwaggerUI within the same web service under a `/docs` endpoint.
