package au.com.belong.utils;

import static java.util.stream.Collectors.toList;

import au.com.belong.viapi.data.Customer;
import au.com.belong.viapi.data.CustomerContact;
import java.util.Arrays;
import java.util.List;

public class Helper {
  public static List<Customer> sampleCustomers(String... args) {
    return Arrays.stream(args).map(Customer::new).collect(toList());
  }

  public static void addContacts(Customer customer, String... phoneNumbers) {
    customer.setContacts(
        Arrays.stream(phoneNumbers).map(ph -> new CustomerContact(customer, ph)).collect(toList()));
  }
}
