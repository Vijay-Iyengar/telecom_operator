package au.com.belong.viapi.services;

import static au.com.belong.utils.Helper.addContacts;
import static au.com.belong.utils.Helper.sampleCustomers;
import static au.com.belong.viapi.constants.Messages.CUSTOMER_ID_CANNOT_BE_NULL;
import static au.com.belong.viapi.constants.Messages.INVALID_CUSTOMER_ID;
import static au.com.belong.viapi.constants.Messages.INVALID_PHONE_NUMBER;
import static au.com.belong.viapi.constants.Messages.PHONE_NUMBER_CANNOT_BE_EMPTY;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.mockito.Mockito.when;

import au.com.belong.viapi.data.Customer;
import au.com.belong.viapi.exceptions.InvalidCustomerException;
import au.com.belong.viapi.exceptions.InvalidPhoneNumberException;
import au.com.belong.viapi.repositories.CustomerRepository;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;
import lombok.SneakyThrows;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
class CustomerContactServiceTest {

  private static final long DEFAULT_ID = 1L;
  @Mock CustomerRepository customerRepository;
  @InjectMocks CustomerContactService sut;

  static Stream<String> blankStrings() {
    return Stream.of("", "   ", null);
  }

  @Nested
  @DisplayName("Tests to activate a PhoneNumber For 1 Customer")
  class TestActivatePhoneNumber {

    @Test
    @DisplayName("Given valid customerNumber and valid phoneNumber, Should true")
    void testExistingCustomerIdActivatePhoneNumber() {

      // Arrange
      var phoneNumbers = List.of("0425441900");
      var customer = sampleCustomers("Mike").get(0);
      customer.setId(DEFAULT_ID);
      addContacts(customer, phoneNumbers.get(0));
      when(customerRepository.findById(DEFAULT_ID)).thenReturn(Optional.of(customer));
      var before = customer.getContacts().get(0).isActive();

      // Act
      sut.activatePhoneNumberForCustomer(DEFAULT_ID, "0425441900");
      var actual = customer.getContacts().get(0).isActive();
      // Assert
      assertThat(before).isFalse();
      assertThat(actual).isTrue();
    }

    @Test
    @DisplayName("Given nonExisting customerNumber, Should throw InvalidCustomerException")
    void testNonExistingCustomerIdActivatePhoneNumber() {

      // Act
      assertThatThrownBy(() -> sut.activatePhoneNumberForCustomer(DEFAULT_ID, "04 22 34324234"))
          .isInstanceOf(InvalidCustomerException.class)
          .hasMessage(INVALID_CUSTOMER_ID);
    }

    @ParameterizedTest
    @SneakyThrows
    @MethodSource("au.com.belong.viapi.services.CustomerContactServiceTest#blankStrings")
    @DisplayName("Given Invalid PhoneNumber to activate,Should throw IllegalArgumentException")
    void testInvalidPhoneNumberToActivate(String ph) {
      assertThatThrownBy(() -> sut.activatePhoneNumberForCustomer(DEFAULT_ID, ph))
          .isInstanceOf(IllegalArgumentException.class)
          .hasMessage(PHONE_NUMBER_CANNOT_BE_EMPTY);
    }

    @Test
    @DisplayName("Given null customer Number,Should throw IllegalArgumentException")
    void testNullCustomerForActivatePhoneNumber() {
      assertThatThrownBy(() -> sut.activatePhoneNumberForCustomer(null, "04 25441343"))
          .isInstanceOf(IllegalArgumentException.class)
          .hasMessage(CUSTOMER_ID_CANNOT_BE_NULL);
    }

    @Test
    @DisplayName("Given nonExisting phoneNumber, Should throw InvalidPhoneNumberException")
    void testNonExistingPhNumActivatePhoneNumber() {

      // Arrange
      var phoneNumbers = List.of("0425441900");
      var customer = sampleCustomers("Mike").get(0);
      customer.setId(DEFAULT_ID);
      addContacts(customer, phoneNumbers.get(0));
      when(customerRepository.findById(DEFAULT_ID)).thenReturn(Optional.of(customer));

      // Act
      assertThatThrownBy(
              () -> sut.activatePhoneNumberForCustomer(DEFAULT_ID, "incorrect_phone_Number"))
          .isInstanceOf(InvalidPhoneNumberException.class)
          .hasMessage(INVALID_PHONE_NUMBER);
    }
  }

  @Nested
  @DisplayName("Tests for get All PhoneNumbers For 1 Customer")
  class TestGetAllPhoneNumbersForACustomer {

    @Test
    @DisplayName(
        "Given 3 phoneNumbers, for a customers,Should return all phoneNumbers for that customer")
    void testGetAllPhoneNumbersForCustomerWithMultipleContact() {

      // Arrange
      var expected = List.of("0425441900", "0425441901", "0425441902");
      var customer = sampleCustomers("Mike").get(0);
      customer.setId(DEFAULT_ID);
      addContacts(customer, expected.get(0), expected.get(1), expected.get(2));
      when(customerRepository.findById(DEFAULT_ID)).thenReturn(Optional.of(customer));

      // Act
      Optional<List<String>> actual = sut.getAllPhoneNumbersForCustomer(DEFAULT_ID);

      // Assert
      assertThat(actual).isPresent();
      assertThat(actual.get()).containsExactlyElementsOf(expected);
    }

    @Test
    @DisplayName(
        "Given null customer Number for get all phone numbers for a customer ,Should throw IllegalArgumentException")
    void testNullCustomerGetContacts() {
      assertThatThrownBy(() -> sut.getAllPhoneNumbersForCustomer(null))
          .isInstanceOf(IllegalArgumentException.class)
          .hasMessage(CUSTOMER_ID_CANNOT_BE_NULL);
    }
  }

  @Nested
  @DisplayName("Tests for get All PhoneNumbers")
  class TestGetAllPhoneNumbers {

    @Test
    @DisplayName("Given 3 phoneNumbers, for 3 customers, Should return 3 phoneNumbers")
    void testGetAllCustomers() {

      // Arrange
      var phoneNumbers = List.of("0425441900", "0425441901", "0425441902");
      var expected = sampleCustomers("Mike", "Jane", "Gary");
      addContacts(expected.get(0), phoneNumbers.get(0));
      addContacts(expected.get(1), phoneNumbers.get(1));
      addContacts(expected.get(2), phoneNumbers.get(2));
      when(customerRepository.findAll()).thenReturn(expected);

      // Act
      List<Customer> actual = sut.getAllPhoneNumbers();

      // Assert
      assertThat(actual).isEqualTo(expected);
    }

    @Test
    @DisplayName("Given 0 phoneNumbers, for 3 customers, Should return 0 phoneNumbers")
    void testGetAllCustomersForEmptyContact() {

      // Arrange
      var customers = sampleCustomers("Mike", "Jane", "Gary");
      when(customerRepository.findAll()).thenReturn(customers);

      // Act
      List<Customer> actual = sut.getAllPhoneNumbers();

      // Assert
      assertThat(actual).isEmpty();
    }

    @Test
    @DisplayName("Given No phoneNumbers, for 1 customers, Should return 0 phoneNumbers")
    void testGetAllCustomersForCustomerWithNoContact() {

      // Arrange
      var customers = sampleCustomers("Mike", "Jane", "Gary");
      when(customerRepository.findAll()).thenReturn(customers);

      // Act
      List<Customer> actual = sut.getAllPhoneNumbers();

      // Assert
      assertThat(actual).isEmpty();
    }
  }
}
