package au.com.belong.viapi.controllers;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import au.com.belong.viapi.requests.PhoneActivateRequest;
import au.com.belong.viapi.services.CustomerContactService;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.List;
import java.util.Optional;
import lombok.SneakyThrows;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

@WebMvcTest(CustomerContactsController.class)
class CustomerContactsControllerTest {
  private static final String CUSTOMER_ID = "/{customerId}";
  private static final String CUSTOMERS_PART = "/customers";
  private static final String PHONE_PART = "/phone-numbers";
  private final ObjectMapper mapper = new ObjectMapper();

  @MockBean CustomerContactService service;

  @Autowired private MockMvc mockMvc;

  @Test
  @SneakyThrows
  @DisplayName("Given valid 'getAllPhoneNumbers' for a customer request, Should return 200")
  void validGetAllPhNumForCustomerReturn200() {
    // Arrange
    var BASE_URL = CUSTOMERS_PART + CUSTOMER_ID + PHONE_PART;
    var expected = List.of("000", "001");
    when(service.getAllPhoneNumbersForCustomer(1L)).thenReturn(Optional.of(expected));
    // Act
    this.mockMvc
        .perform(get(BASE_URL, 1).contentType(APPLICATION_JSON))

        // Assert
        .andExpect(status().isOk())
        .andExpect(content().json(mapper.writeValueAsString(expected)));
  }

  @Test
  @SneakyThrows
  @DisplayName("Given valid 'customerId' and Valid phone number, Should return 201")
  void validActivateReturn201() {
    // Arrange
    var BASE_URL = CUSTOMERS_PART + CUSTOMER_ID + PHONE_PART +"/activate";
    var request = mapper.writeValueAsString(new PhoneActivateRequest("000"));
    doNothing().when(service).activatePhoneNumberForCustomer(1L, "000");
    // Act
    this.mockMvc
        .perform(put(BASE_URL, 1).contentType(APPLICATION_JSON).content(request))

        // Assert
        .andExpect(status().isAccepted());
  }

  @Test
  @SneakyThrows
  @DisplayName("Given valid 'getAllPhoneNumbers' request, Should return 200")
  void validGetAllPhoneNumbersShouldReturn200() {
    // Arrange
    var BASE_URL = CUSTOMERS_PART + PHONE_PART;

    // Act
    this.mockMvc
        .perform(get(BASE_URL).contentType(APPLICATION_JSON))

        // Assert
        .andExpect(status().isOk());
  }

  @Test
  @SneakyThrows
  @DisplayName("Given Invalid 'customerId', Should return 400")
  void invalidCustomerIdReturn400() {
    // Arrange
    var BASE_URL = CUSTOMERS_PART + CUSTOMER_ID + PHONE_PART;
    // Act
    this.mockMvc
        .perform(get(BASE_URL, "abc").contentType(APPLICATION_JSON))

        // Assert
        .andExpect(status().isBadRequest());
  }
  @Test
  @SneakyThrows
  @DisplayName("Given Non Existing 'customerId', Should return 404")
  void NonExistingCustomerIdReturn400() {
    // Arrange
    var BASE_URL = CUSTOMERS_PART + CUSTOMER_ID + PHONE_PART;
    when(service.getAllPhoneNumbersForCustomer(1L)).thenReturn(Optional.empty());

    // Act
    this.mockMvc
        .perform(get(BASE_URL, "1").contentType(APPLICATION_JSON))

        // Assert
        .andExpect(status().isNotFound());
  }
  @Test
  @SneakyThrows
  @DisplayName("Given Invalid 'phone Number', Should return 400")
  void invalidPhoneNumberReturn400() {
    // Arrange
    var BASE_URL = CUSTOMERS_PART + CUSTOMER_ID + PHONE_PART +"/activate";
    var request = mapper.writeValueAsString(new PhoneActivateRequest(""));
    // Act
    this.mockMvc
        .perform(put(BASE_URL, 1).contentType(APPLICATION_JSON).content(request))

        // Assert
        .andExpect(status().isBadRequest());
  }
}
