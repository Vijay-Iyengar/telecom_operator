package au.com.belong.viapi.repositories;

import static org.assertj.core.api.Assertions.assertThat;

import au.com.belong.viapi.data.CustomerContact;
import java.util.List;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;

@DataJpaTest
@ActiveProfiles("test")
@DirtiesContext(classMode = ClassMode.BEFORE_EACH_TEST_METHOD)
class CustomerRepositoryTest {
  @Autowired private CustomerRepository sut;
  @Autowired private TestEntityManager entityManager;

  @Test
  @Sql(scripts = {"/import_customer.sql", "/import_customer_contact.sql"})
  @DisplayName("Given a valid customer, should return all contacts")
  void testGetCustomerContact() {
    // Act
    var customer =
        sut.findById(1L).orElseThrow(() -> new RuntimeException("Could not read Customer"));
    List<CustomerContact> contacts = customer.getContacts();

    // Assert
    assertThat(contacts).hasSize(1);
  }

  @Test
  @Sql(scripts = {"/import_customer.sql", "/import_customer_contact.sql"})
  @DisplayName("Given a valid customer and valid phone number, should activate")
  void testActivateForCustomerContact() {

    // Arrange
    var customer =
        sut.findById(3L).orElseThrow(() -> new RuntimeException("Could not read Customer"));
    // Act
    var contact = customer.getContacts().get(0);
    contact.setActive(true);
    sut.save(customer);
    entityManager.flush();

    var afterUpdate =
        sut.findById(3L).orElseThrow(() -> new RuntimeException("Could not read Customer"));
    entityManager.flush();

    // Assert
    var afterUpdateStatus = afterUpdate.getContacts().get(0);
    assertThat(afterUpdateStatus.isActive()).isTrue();
  }
}
