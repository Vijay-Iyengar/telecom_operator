package au.com.belong.viapi.repositories;

import static org.assertj.core.api.Assertions.assertThat;

import au.com.belong.viapi.data.CustomerContact;
import java.util.List;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;

@DataJpaTest
@ActiveProfiles("test")
@DirtiesContext(classMode = ClassMode.BEFORE_EACH_TEST_METHOD)
class CustomerContactRepositoryTest {

  @Autowired private CustomerRepository customerRepository;
  @Autowired private CustomerContactRepository sut;

  @Test
  @Sql(scripts = {"/import_customer.sql"})
  @DisplayName("Given valid contact for 1 customer, should save to database")
  void testInsertCustomerContact() {
    // Arrange
    var customer =
        customerRepository
            .findById(1L)
            .orElseThrow(() -> new RuntimeException("Could not read Customer"));
    String primaryContact = "+61 425441873";
    CustomerContact contact = new CustomerContact(customer, primaryContact);

    // Act
    sut.save(contact);
    List<CustomerContact> customerContacts = sut.findAll();

    // Assert
    assertThat(customerContacts).hasSize(1);
    assertThat(customerContacts.get(0)).isEqualTo(contact);
  }
}
