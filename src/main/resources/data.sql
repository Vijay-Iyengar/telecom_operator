DROP TABLE IF EXISTS customer_contact;
DROP TABLE IF EXISTS customer;

CREATE TABLE customer
(
    id   BIGINT IDENTITY NOT NULL PRIMARY KEY,
    name varchar(255)    not null
);

CREATE TABLE customer_contact
(
    id          BIGINT IDENTITY NOT NULL PRIMARY KEY,
    customer_id BIGINT,
    phone       varchar(100)    not null,
    active      boolean default false,
    foreign key (customer_id) REFERENCES customer (id)

);


INSERT INTO customer (name)
VALUES ('Mason');
INSERT INTO customer (name)
VALUES ('Baker');
INSERT INTO customer (name)
VALUES ('Randall');
INSERT INTO customer (name)
VALUES ('Steven');
INSERT INTO customer (name)
VALUES ('Palmer');

INSERT INTO customer_contact (customer_id, phone, active)
VALUES (1, '(04) 9720 3766', true);
INSERT INTO customer_contact (customer_id, phone, active)
VALUES (2, '(02) 3793 7597', true);
INSERT INTO customer_contact (customer_id, phone, active)
VALUES (2, '(01) 0161 1510', false);
INSERT INTO customer_contact (customer_id, phone, active)
VALUES (3, '(09) 9088 8349', false);
INSERT INTO customer_contact (customer_id, phone, active)
VALUES (3, '(03) 7966 4217', false);
INSERT INTO customer_contact (customer_id, phone, active)
VALUES (3, '(04) 9591 5453', false);
INSERT INTO customer_contact (customer_id, phone, active)
VALUES (4, '(03) 4429 6917', false);
INSERT INTO customer_contact (customer_id, phone, active)
VALUES (4, '(04) 7188 6063', false);
INSERT INTO customer_contact (customer_id, phone, active)
VALUES (4, '(05) 8959 8463', false);
INSERT INTO customer_contact (customer_id, phone, active)
VALUES (4, '(02) 2613 5970', false);
INSERT INTO customer_contact (customer_id, phone, active)
VALUES (5, '(01) 8360 6194', false);
INSERT INTO customer_contact (customer_id, phone, active)
VALUES (5, '(04) 8429 8851', false);
INSERT INTO customer_contact (customer_id, phone, active)
VALUES (5, '(02) 0049 2626', false);
INSERT INTO customer_contact (customer_id, phone, active)
VALUES (5, '(07) 8305 6804', false);
INSERT INTO customer_contact (customer_id, phone, active)
VALUES (5, '(04) 7963 0757', false);
