package au.com.belong.viapi.requests;

import javax.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PhoneActivateRequest {

  @NotBlank(message = "Phone number is mandatory")
  private String phoneNumber;
}
