package au.com.belong.viapi.data;

import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity
public class Customer {

  @OneToMany(mappedBy = "customer", cascade = CascadeType.ALL, orphanRemoval = true)
  List<CustomerContact> contacts;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  private String name;

  public Customer(String name) {
    this.name = name;
    this.contacts = List.of();
  }

  public void addContact(CustomerContact contact) {
    contacts.add(contact);
    contact.setCustomer(this);
  }

  public void removeContact(CustomerContact contact) {
    contacts.remove(contact);
    contact.setCustomer(null);
  }
}
