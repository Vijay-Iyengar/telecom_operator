package au.com.belong.viapi.data;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@Table(name = "customer_contact")
@NoArgsConstructor
@Entity
public class CustomerContact {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "customer_id", nullable = false)
  @ToString.Exclude
  private Customer customer;

  private String phone;

  private boolean active;

  public CustomerContact(Customer customer, String phone) {
    this.customer = customer;
    this.phone = phone;
  }
}
