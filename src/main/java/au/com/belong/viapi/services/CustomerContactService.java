package au.com.belong.viapi.services;

import static au.com.belong.viapi.constants.Messages.CUSTOMER_ID_CANNOT_BE_NULL;
import static au.com.belong.viapi.constants.Messages.INVALID_CUSTOMER_ID;
import static au.com.belong.viapi.constants.Messages.INVALID_PHONE_NUMBER;
import static au.com.belong.viapi.constants.Messages.PHONE_NUMBER_CANNOT_BE_EMPTY;
import static java.util.stream.Collectors.toList;

import au.com.belong.viapi.data.Customer;
import au.com.belong.viapi.data.CustomerContact;
import au.com.belong.viapi.exceptions.InvalidCustomerException;
import au.com.belong.viapi.exceptions.InvalidPhoneNumberException;
import au.com.belong.viapi.repositories.CustomerRepository;
import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

@Service
@Slf4j
@RequiredArgsConstructor
public class CustomerContactService {

  private final CustomerRepository customerRepository;

  public List<Customer> getAllPhoneNumbers() {

    return customerRepository.findAll().stream()
        .filter(customer -> !customer.getContacts().isEmpty())
        .collect(toList());
  }

  public Optional<List<String>> getAllPhoneNumbersForCustomer(Long customerId) {
    Assert.notNull(customerId, CUSTOMER_ID_CANNOT_BE_NULL);

    Optional<Customer> maybeCustomer = customerRepository.findById(customerId);

    if (maybeCustomer.isEmpty()) {
      return Optional.empty();
    }

    List<String> contacts =
        maybeCustomer.stream()
            .flatMap(customer -> customer.getContacts().stream())
            .map(CustomerContact::getPhone)
            .collect(toList());

    return Optional.of(contacts);
  }

  public void activatePhoneNumberForCustomer(Long customerId, String phoneNumber) {
    Assert.hasText(phoneNumber, PHONE_NUMBER_CANNOT_BE_EMPTY);
    Assert.notNull(customerId, CUSTOMER_ID_CANNOT_BE_NULL);

    var customer =
        customerRepository
            .findById(customerId)
            .orElseThrow(() -> new InvalidCustomerException(INVALID_CUSTOMER_ID));
    var contact =
        customer.getContacts().stream()
            .filter(cc -> phoneNumber.equals(cc.getPhone()))
            .findFirst()
            .orElseThrow(() -> new InvalidPhoneNumberException(INVALID_PHONE_NUMBER));

    contact.setActive(true);
    customerRepository.save(customer);
  }
}
