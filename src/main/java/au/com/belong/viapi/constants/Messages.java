package au.com.belong.viapi.constants;

public class Messages {
  public static final String PHONE_NUMBER_CANNOT_BE_EMPTY = "PhoneNumber cannot be empty";
  public static final String INVALID_CUSTOMER_ID = "Invalid Customer Id";
  public static final String INVALID_PHONE_NUMBER = "Invalid Phone Number";
  public static final String CUSTOMER_ID_CANNOT_BE_NULL = "CustomerId cannot be null";

  private Messages() {
    /*Never Called*/
  }
}
