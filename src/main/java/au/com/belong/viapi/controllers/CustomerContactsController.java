package au.com.belong.viapi.controllers;

import au.com.belong.viapi.exceptions.InvalidCustomerException;
import au.com.belong.viapi.requests.PhoneActivateRequest;
import au.com.belong.viapi.responses.AllPhoneNumberResponse;
import au.com.belong.viapi.services.CustomerContactService;
import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class CustomerContactsController {
  private final CustomerContactService service;

  @GetMapping("/customers/phone-numbers")
  @ResponseStatus(HttpStatus.OK)
  public AllPhoneNumberResponse readAllPhoneNumbers() {
    return new AllPhoneNumberResponse(service.getAllPhoneNumbers());
  }

  @GetMapping("/customers/{customer-id}/phone-numbers")
  @ResponseStatus(HttpStatus.OK)
  public List<String> readAllPhoneNumbersForCustomer(@PathVariable("customer-id") Long customerId) {
    return service
        .getAllPhoneNumbersForCustomer(customerId)
        .orElseThrow(InvalidCustomerException::new);
  }

  @PutMapping("/customers/{customer-id}/phone-numbers/activate")
  @ResponseStatus(HttpStatus.ACCEPTED)
  public void activatePhoneNumber(
      @PathVariable("customer-id") @NotNull(message = "CustomerId is Mandatory") Long customerId,
      @RequestBody @Valid PhoneActivateRequest request) {
    service.activatePhoneNumberForCustomer(customerId, request.getPhoneNumber());
  }
}
