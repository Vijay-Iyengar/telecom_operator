package au.com.belong.viapi.responses;

import au.com.belong.viapi.data.Customer;
import au.com.belong.viapi.data.CustomerContact;
import java.util.List;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class AllPhoneNumberResponse {
  List<CustomerResponse> customerList;

  public AllPhoneNumberResponse(List<Customer> customers) {
    this.customerList =
        customers.stream()
            .map(
                customer ->
                    new CustomerResponse(
                        customer.getId(),
                        customer.getName(),
                        customer.getContacts().stream()
                            .map(CustomerContact::getPhone)
                            .collect(Collectors.toList())))
            .collect(Collectors.toList());
  }

  @Data
  @AllArgsConstructor
  static class CustomerResponse {
    private Long id;
    private String name;
    private List<String> phoneNumbers;
  }
}
