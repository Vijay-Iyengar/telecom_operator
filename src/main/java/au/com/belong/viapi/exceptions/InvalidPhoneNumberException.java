package au.com.belong.viapi.exceptions;

import static au.com.belong.viapi.constants.Messages.INVALID_PHONE_NUMBER;

public class InvalidPhoneNumberException extends RuntimeException {

  public InvalidPhoneNumberException() {
    this(INVALID_PHONE_NUMBER);
  }

  public InvalidPhoneNumberException(String message) {
    super(message);
  }
}
