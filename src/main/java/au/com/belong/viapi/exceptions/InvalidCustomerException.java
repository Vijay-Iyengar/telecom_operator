package au.com.belong.viapi.exceptions;

import static au.com.belong.viapi.constants.Messages.INVALID_CUSTOMER_ID;

public class InvalidCustomerException extends RuntimeException {

  public InvalidCustomerException() {
    this(INVALID_CUSTOMER_ID);
  }

  public InvalidCustomerException(String message) {
    super(message);
  }
}
